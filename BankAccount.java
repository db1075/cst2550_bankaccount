import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.Condition;
public class BankAccount
{
    private double balance;
    private Lock b_l = new ReentrantLock();
    private Condition blw_0 = b_l.newCondition();
    public BankAccount()
    {
        balance = 0;
    }

    public void deposit(double amount)
    {
	b_l.lock();
	System.out.print("Depositing " + amount);
        double newBalance = balance + amount;
	System.out.println(", new balance is " + newBalance);
        balance = newBalance;
	blw_0.signal();
	b_l.unlock();
    }

    public void withdraw(double amount)
    {
	b_l.lock();
	try{

            while(balance < amount)
	    blw_0.await();
       	    System.out.print("Withdrawing " + amount);
            double newBalance = balance - amount;
            System.out.println(", new balance is " + newBalance);
            balance = newBalance;
	}catch(InterruptedException ex){
	    
	}finally{
	    b_l.unlock();
	}
	
    }

    public double getBalance()
    {
        return balance;
    }
}
